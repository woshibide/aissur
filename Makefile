# Compiler
CC = gcc

# Compiler flags
CFLAGS = -Wall -Wextra -std=c99

# Directories
RAYLIB_DIR = ../raylib
INCLUDE_DIR = $(RAYLIB_DIR)/src

# Libraries
LIBS = -L$(RAYLIB_DIR)/src -lraylib -lGL -lm -lpthread -ldl -lrt -lX11

# Source files
SRC = main.c

# Object files
OBJ = $(SRC:.c=.o)

# Executable name
EXEC = VOVKA 

# Rule to build the executable
$(EXEC): $(OBJ)
	$(CC) $(OBJ) -o $(EXEC) $(LIBS)

# Rule to build object files from source files
%.o: %.c
	$(CC) -c $(CFLAGS) -I$(INCLUDE_DIR) $< -o $@

# Clean rule
clean:
	rm -f $(OBJ) $(EXEC)

